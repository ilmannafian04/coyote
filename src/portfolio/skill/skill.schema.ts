import { Schema } from 'mongoose';

const SkillSchema = new Schema({
    name: String,
    proficiency: String,
    category: String,
});

export default SkillSchema;
