import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
class Skill {
    @Field()
    _id: string;

    @Field()
    name: string;

    @Field()
    proficiency: string;

    @Field()
    category: string;
}

export default Skill;
