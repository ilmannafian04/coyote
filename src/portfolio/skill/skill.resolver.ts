import { UseGuards } from '@nestjs/common';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';

import NewSkillInput from './dto/new-skill.input';
import SkillArgs from './dto/skill.args';
import Skill from './skill.model';
import SkillService from './skill.service';
import GqlAuthGuard from '../../auth/gql-auth.guard';

@Resolver(Skill)
export default class SkillResolver {
    constructor(private skillService: SkillService) {}

    @Query(() => [Skill])
    async skills(): Promise<Skill[]> {
        return this.skillService.findAll();
    }

    @UseGuards(GqlAuthGuard)
    @Mutation(() => Skill)
    async addSkill(@Args('newSkillInput') newSkillInput: NewSkillInput): Promise<Skill> {
        return this.skillService.createSkill(newSkillInput);
    }

    @Query(() => [Skill])
    async findSkills(@Args() skillArgs: SkillArgs): Promise<Skill[]> {
        return this.skillService.filterSkills(skillArgs);
    }
}
