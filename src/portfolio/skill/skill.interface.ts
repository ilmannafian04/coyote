import { Document } from 'mongoose';

interface Skill extends Document {
    readonly name: string;
    readonly proficiency: string;
    readonly category: string;
}

export default Skill;
