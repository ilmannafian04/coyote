import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import SkillArgs from './dto/skill.args';
import Skill from './skill.interface';
import NewLanguageInput from '../language/dto/new-language.input';

@Injectable()
export default class SkillService {
    constructor(@InjectModel('Skill') private readonly skillModel: Model<Skill>) {}

    async findAll(): Promise<Skill[]> {
        return this.skillModel.find();
    }

    async createSkill(newLanguageInput: NewLanguageInput): Promise<Skill> {
        const skill = new this.skillModel(newLanguageInput);
        return skill.save();
    }

    async filterSkills(skillArgs: SkillArgs): Promise<Skill[]> {
        return this.skillModel.find(skillArgs);
    }
}
