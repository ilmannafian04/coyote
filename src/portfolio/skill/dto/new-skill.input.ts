import { Field, InputType } from '@nestjs/graphql';

@InputType()
export default class NewSkillInput {
    @Field()
    name: string;

    @Field()
    proficiency: string;

    @Field()
    category: string;
}
