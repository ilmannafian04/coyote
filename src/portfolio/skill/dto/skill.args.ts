import { ArgsType, Field } from '@nestjs/graphql';

@ArgsType()
export default class SkillArgs {
    @Field({ nullable: true })
    proficiency?: string;

    @Field({ nullable: true })
    category?: string;
}
