import { Schema } from 'mongoose';

const LanguageSchema = new Schema({
    name: String,
});

export default LanguageSchema;
