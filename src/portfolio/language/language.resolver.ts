import { UseGuards } from '@nestjs/common';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';

import NewLanguageInput from './dto/new-language.input';
import Language from './language.model';
import LanguageService from './language.service';
import GqlAuthGuard from '../../auth/gql-auth.guard';

@Resolver(Language)
export default class LanguageResolver {
    constructor(private languageService: LanguageService) {}

    @Query(() => [Language])
    async languages(): Promise<Language[]> {
        return this.languageService.findAll();
    }

    @UseGuards(GqlAuthGuard)
    @Mutation(() => Language)
    async addLanguage(@Args('newLanguageData') newLanguageData: NewLanguageInput): Promise<Language> {
        return this.languageService.createLanguage(newLanguageData);
    }
}
