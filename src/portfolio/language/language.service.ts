import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import NewLanguageInput from './dto/new-language.input';
import Language from './language.interface';

@Injectable()
export default class LanguageService {
    constructor(@InjectModel('Language') private readonly languageModel: Model<Language>) {}

    async findAll(): Promise<Language[]> {
        return this.languageModel.find().exec();
    }

    async createLanguage(newLanguageData: NewLanguageInput): Promise<Language> {
        const language = new this.languageModel(newLanguageData);
        return language.save();
    }
}
