import { Field, InputType } from '@nestjs/graphql';

@InputType()
class NewLanguageInput {
    @Field()
    name: string;
}

export default NewLanguageInput;
