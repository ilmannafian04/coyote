import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
class Language {
    @Field()
    _id: string;

    @Field()
    name: string;
}

export default Language;
