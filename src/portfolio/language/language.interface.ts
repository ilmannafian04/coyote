import { Document } from 'mongoose';

interface Language extends Document {
    readonly name: string;
}

export default Language;
