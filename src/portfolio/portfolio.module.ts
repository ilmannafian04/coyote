import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import LanguageResolver from './language/language.resolver';
import LanguageSchema from './language/language.schema';
import LanguageService from './language/language.service';
import SkillResolver from './skill/skill.resolver';
import SkillSchema from './skill/skill.schema';
import SkillService from './skill/skill.service';

@Module({
    imports: [
        MongooseModule.forFeature([
            { name: 'Language', schema: LanguageSchema },
            { name: 'Skill', schema: SkillSchema },
        ]),
    ],
    providers: [LanguageService, LanguageResolver, SkillService, SkillResolver],
})
export default class PortfolioModule {}
