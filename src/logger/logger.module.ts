import { Module } from '@nestjs/common';
import CoyoteLogger from './coyote.logger';

@Module({
    providers: [CoyoteLogger],
    exports: [CoyoteLogger],
})
export class LoggerModule {}
