import * as Joi from '@hapi/joi';
import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { GraphQLModule } from '@nestjs/graphql';
import { MongooseModule } from '@nestjs/mongoose';

import AppController from './app.controller';
import { AuthModule } from './auth/auth.module';
import { LesserModule } from './lesser/lesser.module';
import { LoggerModule } from './logger/logger.module';
import PortfolioModule from './portfolio/portfolio.module';

@Module({
    imports: [
        MongooseModule.forRootAsync({
            imports: [ConfigModule],
            useFactory: async (configService: ConfigService) => ({
                uri: configService.get('MONGO_URL'),
                useNewUrlParser: true,
                useUnifiedTopology: true,
            }),
            inject: [ConfigService],
        }),
        GraphQLModule.forRootAsync({
            imports: [ConfigModule],
            useFactory: async (configService: ConfigService) => ({
                debug: configService.get('NODE_ENV') === 'development',
                playground: configService.get('NODE_ENV') === 'development',
                autoSchemaFile: configService.get('NODE_ENV') === 'development' ? true : 'src/graphql',
                context: ({ req }) => ({ req }),
            }),
            inject: [ConfigService],
        }),
        ConfigModule.forRoot({
            validationSchema: Joi.object({
                MONGO_URL: Joi.string()
                    .uri({ scheme: ['mongodb', 'mongodb+srv'] })
                    .required(),
                SECRET_KEY: Joi.string().required(),
                NODE_ENV: Joi.string()
                    .valid('development', 'production')
                    .default('development')
                    .optional(),
                PORT: Joi.number()
                    .default(3000)
                    .optional(),
            }),
            isGlobal: true,
        }),
        PortfolioModule,
        AuthModule,
        LesserModule,
        LoggerModule,
    ],
    controllers: [AppController],
})
export class AppModule {}
