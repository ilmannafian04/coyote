import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';

import { AppModule } from './app.module';

async function bootstrap() {
    const app = await NestFactory.create(AppModule, { cors: true });
    const config = app.get(ConfigService);
    await app.listen(config.get('PORT'), '0.0.0.0');
}

bootstrap();
