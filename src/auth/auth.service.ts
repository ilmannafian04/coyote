import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { compare } from 'bcrypt';

import LesserService from '../lesser/lesser.service';

@Injectable()
export class AuthService {
    constructor(private lesserService: LesserService, private jwtService: JwtService) {}

    async validateLesser(username: string, password: string): Promise<any> {
        return this.lesserService.findOne(username).then(lesser => {
            if (lesser) {
                return compare(password, lesser.password).then(valid => {
                    return valid ? { _id: lesser._id, username: lesser.username } : null;
                });
            } else {
                return null;
            }
        });
    }

    async login(lesser: any) {
        const payload = {
            username: lesser.username,
            sub: lesser._id,
        };
        // eslint-disable-next-line @typescript-eslint/camelcase
        return { access_token: this.jwtService.sign(payload) };
    }
}
