import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-local';

import { AuthService } from './auth.service';

@Injectable()
export default class LocalStrategy extends PassportStrategy(Strategy) {
    constructor(private authService: AuthService) {
        super();
    }

    async validate(username: string, password: string): Promise<any> {
        return this.authService
            .validateLesser(username, password)
            .then(lesser => {
                if (lesser) {
                    return lesser;
                } else {
                    throw new UnauthorizedException();
                }
            })
            .catch(error => {
                console.log(error);
            });
    }
}
