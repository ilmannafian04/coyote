import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import Lesser from './interfaces/lesser.interface';

@Injectable()
export default class LesserService {
    constructor(@InjectModel('Lesser') private readonly lesserModel: Model<Lesser>) {}

    async findOne(username: string): Promise<Lesser | undefined> {
        return this.lesserModel.findOne({ username: username });
    }
}
