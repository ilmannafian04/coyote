import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import LesserService from './lesser.service';
import LesserSchema from './schemas/lesser.schema';

@Module({
    imports: [MongooseModule.forFeature([{ name: 'Lesser', schema: LesserSchema }])],
    providers: [LesserService],
    exports: [LesserService],
})
export class LesserModule {}
