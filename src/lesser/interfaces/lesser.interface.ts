import { Document } from 'mongoose';

interface Lesser extends Document {
    username: string;
    password: string;
}

export default Lesser;
