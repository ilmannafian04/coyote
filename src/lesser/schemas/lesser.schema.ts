import { Schema } from 'mongoose';

const LesserSchema = new Schema({
    username: String,
    password: String,
});

export default LesserSchema;
