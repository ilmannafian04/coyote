FROM node:alpine

ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}
WORKDIR /usr/src/app
COPY . .
CMD ["npm", "run", "start:prod"]
